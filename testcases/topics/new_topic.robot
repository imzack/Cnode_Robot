*** Settings ***
Documentation       用户发布话题成功
Resource        ../../resources/user_resource.robot
Resource        ../../resources/topic_resource.robot
Resource        ../../resources/common.robot

Suite Setup     打开浏览器到首页
Suite Teardown  关闭浏览器
Test Template   用户登录成功并发布话题

*** Test Cases ***      标题        内容
正常发布话题            helloworld123   你好！


*** Keywords ***
用户登录成功并发布话题
    [Arguments]     ${title}        ${content}
    预先条件用户登录成功
    点击发布话题
    用户发布话题，标题和内容分别为  ${title}    ${content}
    用户发布话题应该成功    ${title}    ${content}