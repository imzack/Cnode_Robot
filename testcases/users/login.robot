*** Settings ***
Documentation   主要测试用户登录
Resource    ../../resources/user_resource.robot         # resource 文件引用  注意使用的是相对路径
Resource    ../../resources/topic_resource.robot
Suite Setup         打开浏览器到首页        #测试套件运行之前执行的操作
Suite Teardown      关闭浏览器             #测试套件运行之后执行的操作  所有的测试用例执行完成之后执行
Test Setup          Reload Page             #每一个测试用例执行之前的操作 刷新页面
Test Teardown       Delete All Cookies      #每一个测试用例执行之后的操作 删除所有的cookie
*** Test Cases ***

用户登录成功
    [Tags]      冒烟测试
    点击登录链接
    使用用户名密码进行用户登录      testuser10      123456
    用户应该登录成功        testuser10
   

用户登录失败1
    [Tags]      回归测试    冒烟测试
    点击登录链接
    使用用户名密码进行用户登录      testuser10      1212
    用户应该登录失败        用户名或密码错误


用户登录失败2
    点击登录链接
    使用用户名密码进行用户登录      ''      1212
    用户应该登录失败        用户名或密码错误

用户登录失败3
    点击登录链接
    使用用户名密码进行用户登录      ''      ''
    用户应该登录失败        用户名或密码错误