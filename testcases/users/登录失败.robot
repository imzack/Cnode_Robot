*** Settings ***
Documentation       用户登录的的场景
Resource            ../../resources/user_resource.robot
Suite Setup         打开浏览器到首页
Suite Teardown      关闭浏览器
Test Setup          刷新页面
# Test Teardown       删除浏览器Cookie
Test Template       用户登录失败


*** Test Cases ***             用户名           密码        错误提示信息
登录使用空的用户名和密码        ${empty}       123456       信息不完整。
登录使用用户名和空的密码        testuser2       ${empty}    信息不完整。
登录使用错误的用户名和密码        11111111       222222     用户名或密码错误
登录使用特殊字符的用户名和密码      "#￥#@#￥@@"  "￥￥##%#@##￥"    用户名或密码错误


*** Keywords ***
用户登录失败
    [Tags]  冒烟测试
    [Arguments]             ${username}     ${password}     ${error_msg}
    点击登录链接
    使用用户名密码进行用户登录  ${username}   ${password}
    用户应该登录失败            ${error_msg}