*** Settings ***
Documentation       使用excel文件进行数据驱动，excel文件为data\data.excel
Library             DataDriver      ../../data/data.csv      dialect=excel_tab
...                 encoding=utf8
Resource            ../../resources/user_resource.robot
Suite Setup         打开浏览器到首页
Suite Teardown      关闭浏览器
Test Setup          刷新页面
Test Teardown       删除浏览器Cookie
Test Template       用户登录成功


*** Test Cases ***
使用${用户名}和${密码}进行登录，登录应该都成功


*** Keywords ***
用户登录成功
    [Arguments]             ${用户名}     ${密码}
    点击登录链接
    使用用户名密码进行用户登录  ${用户名}   ${密码}
    用户应该登录成功    ${用户名}


