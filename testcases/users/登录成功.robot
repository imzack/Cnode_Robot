*** Settings ***
Documentation       用户成功的的场景
Resource            ../../resources/user_resource.robot
Suite Setup         打开浏览器到首页
Suite Teardown      关闭浏览器
Test Setup          刷新页面
Test Teardown       删除浏览器Cookie
Test Template       用户登录成功



*** Test Cases ***             用户名           密码
使用正确的用户名密码登录        testuser1       123456
使用正确的手机号密码登录        testuser2       123456


*** Keywords ***
用户登录成功
    [Arguments]             ${username}     ${password}
    点击登录链接
    使用用户名密码进行用户登录  ${username}   ${password}
    用户应该登录成功    ${username}
