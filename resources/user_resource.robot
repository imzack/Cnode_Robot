*** Settings ***
Documentation   用户相关的操作
Library     SeleniumLibrary


*** Variables ***     # 定义变量值
${baseUrl}      http://39.107.96.138:3000/

*** Keywords ***
打开浏览器到首页
    Open Browser    ${baseUrl}    Chrome    remote_url=http://47.100.175.62:4444/wd/hub
    Maximize Browser Window

关闭浏览器
    Close Window

刷新页面
    Reload Page

删除浏览器Cookie
    Delete All Cookies


点击登录链接
    Click Element   xpath://*[@href="/signin"]

使用用户名密码进行用户登录
    [Arguments]     ${username}     ${password}       # 定义了username password 两个参数
    Input Text  //*[@id="name"]		${username}
    Input Text  //*[@id="pass"]		${password}
    Click Element   xpath://*[@type="submit"]
    

用户应该登录成功
    [Arguments]     ${username}
    Element Text Should Be  //span[@class="user_name"]/a[@class="dark"]     ${username}

用户应该登录失败
    [Arguments]     ${error_tip_msg}
    Element Text Should Be  //strong     ${error_tip_msg}

打开链接地址
    [Arguments]     ${url}
    Go To   ${url}
