*** Settings ***

Documentation   用户相关的操作
Library     SeleniumLibrary
Library     BuiltIn

*** Keywords ***

点击发布话题
    Click Element   xpath://*[@id="create_topic_btn"]

用户发布话题，标题和内容分别为    
    [Arguments]     ${title}        ${content}
    Click Element   xpath://*[@id="tab-value"]
    Click Element   xpath://option[@value="ask"]
    Input Text      xpath://*[@id="title"]      ${title}
    Click Element   xpath://div[@class="CodeMirror-scroll"]
    Set Focus To Element    xpath://div[@class="CodeMirror-scroll"]
    Press Keys      xpath://div[@class="CodeMirror-scroll"]     ${content}
    Click Element   xpath://*[@type="submit"]
    ${currentUrl}   Log Location
    [Return]    ${currentUrl}                             # 返回值，将当前的url地址返回

用户发布话题应该成功
    [Arguments]         ${title}    ${content}
    ${title_text}       Get Text    xpath://span[@class="topic_full_title"]
    ${content_text}     Get Text    xpath://div[@class="markdown-text"]
    Should Be True      '${title_text}'=='${title}'                     # 实际页面上title的值应该与发布话题使用的值一样
    Should Be True      '${content_text}'=='${content}'                 # 实际页面上content的值应该与发布话题使用的值一样
用户发布话题应该失败
    [Arguments]         ${error_msg}
    ${tips_text}        Get Text    xpath://strong
    Should Be True      '${tips_text}'=='${error_msg}'